# HiJson

#### 介绍
一个JSON字符串格式化小工具，原作者藏言（CangYan）
原始地址： http://code.google.com/p/json-view/

#### 编译工具
- NetBeans 6 
- JDK6/JDK7

#### v2.2.0更新
- 修改为按json字符串默认字段顺序格式化

#### 打开方式
- 可直接下载dist目录下的exe文件, 或者在发行版中下载exe文件

![](https://s1.ax1x.com/2020/06/08/thkrL9.png)